<?php
$gmworker = new GearmanWorker();
$gmworker->addServer('127.0.0.1'); //Gearman host

$gmworker->addFunction("JOB_NAME", "working_fn"); //Link with Gearman client by job name and send to working function

	while( $gmworker->work() )
	{
	  if ($gmworker->returnCode() != GEARMAN_SUCCESS)
	  {
		echo "return_code: " . $gmworker->returnCode() . "\n";
		break;
	  }
	}
	
	//Working function
	function working_fn( $job )
    {
		//workers will work from the same job name as client (queue) and get an ID from queue as well
		$lastID = $job->workload();
		echo $lastID."\r\n";
	}	
?>